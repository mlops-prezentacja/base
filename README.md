# Base

A project without any MLOps tools

## How to use
Example on how to use Tree classifier on iris dataset.

1. Download your data
```bash
mkdir data
wget https://gist.githubusercontent.com/netj/8836201/raw/6f9306ad21398ea43cba4f7d537619d0e07d5ae3/iris.csv -O data/iris.csv
```
2. Preprocess dataset
```bash
python3 preprocess.py --dataset data/iris.csv --out_train data/train.csv --out_test data/test.csv
```
3. Train model
```bash
python3 train.py --train_dataset data/train.csv --test_dataset data/test.csv --target_column variety --model_path data/model.pickle --predictions_path data/predictions.csv --max_depth 1
```
4. Evaluate your model
```
python3 evaluate.py --predictions data/predictions.csv --reference data/test.csv --target_column variety --result_file data/results.json
```
